const express = require('express');
const app = express();
const port = process.env.PORT || 1005;
global.Env = process.env.NODE_ENV || 'local'
const logger = require('./server/config/logging')
if (global.Env != 'production') {
    require('babel-core/register');
    require('babel-polyfill');
}
require('./server/config/database')
require('./server/Model/index')(app);
app.listen(port, () => logger.info(`Server running on port ${port}`));
module.exports = app;