const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const config = require('../helpers/jwt.json');
const Joi = require('joi')
let schema = require('../helpers/schema.js')
let verifyToken = require('../config/auth')
let sendResponse = require('../helpers/sendResponse');
router
    .get(['/'], (req, res) => {
        require('../get_token/api/list.js')(req, res, jwt, config);
    })
    // expiresIn: 5400 // expires in 1.5 hours
    // expiresIn: 86400 // expires in 24 hours
module.exports = router;