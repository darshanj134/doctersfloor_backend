const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const config = require('../helpers/jwt.json');
const Joi = require('joi');
let schema = require('../helpers/schema.js')
let sendResponse = require('../helpers/sendResponse');
const userCreateSchema = require('../view/user/schema/userCreateSchema.js');
router
    .post(['/login'], (req, res) => {
        require('../view/user/api/loginValidate.js')(req, res, sendResponse);
    })
    .post(['/create'], (req, res) => {
        // const result = Joi.validate(req.body, userCreateSchema);
        const result = userCreateSchema.validate(req.body);
        console.log(result);
        if (result.error) {
            const response = {
                success: false,
                type: 'response',
                message: result.error.details[0].message
            }
            sendResponse(req, res, response);
        } else {
            require('../view/user/api/create.js')(req, res, sendResponse);
        }
    })
module.exports = router;