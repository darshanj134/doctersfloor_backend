const Joi = require('joi');

const schema = Joi.object().keys({
    utility_board_id        :   Joi.number().required(),
    sub_division_id         :   Joi.number().required(),
    agency_id               :   Joi.number().required(),
    user_id                 :   Joi.number(),
    consumer_account_number :   Joi.number(),
    user_role               :   Joi.string().required(),
    user_type               :   Joi.string().required()
})
module.exports = schema;