'use strict';
module.exports = create;
const jwt = require('jsonwebtoken');
const config = require('../../../helpers/jwt.json');
const shaOne = require('sha1');
const { valid } = require('joi');
const { validate } = require('../../../helpers/schema');

function create(req, res, sendResponse) {
    console.info(req.body);
    let today = new Date();
    let data = {};
    let message = "";
    let success = "";
    let sql = "";
    let pwd = shaOne(req.body.password);
    let insertData = {
        "salutation": req.body.salutation,
        "first_name": req.body.first_name,
        "middle_name": req.body.middle_name,
        "last_name": req.body.last_name,
        "email": req.body.email,
        "password": pwd,
        "is_active": req.body.is_active,
        "created_at": today
    }
    let validate_sql = "SELECT * FROM users where email = '" + req.body.email + "'";
    console.info(validate_sql);
    con.query(validate_sql,
        function(err, resultVal) {
            if (err) {
                logger.info(err);
                let response = {
                    type: "db_error",
                    status: false,
                    error: err
                }
                sendResponse(req, res, response);
            } else {
                if (resultVal.length >= 1) {
                    success = false;
                    data = {};
                    message = "User already Exists";
                    const response = {
                        type: "response",
                        success: success,
                        data: data,
                        message: message
                    };
                    sendResponse(req, res, response);
                } else {
                    sql = "INSERT INTO users SET ?";
                    con.query(sql, insertData, function(error, result) {
                        if (error) {
                            // logger.info(error);
                            let response = {
                                type: "db_error",
                                status: false,
                                error: error
                            }
                            sendResponse(req, res, response);
                        } else {
                            // console.info(result);
                            success = true;
                            data = result;
                            message = "User Added Successfully";
                            const response = {
                                type: "response",
                                success: success,
                                data: data,
                                message: message
                            };
                            sendResponse(req, res, response);
                        }
                    })
                }
            }
        });
}