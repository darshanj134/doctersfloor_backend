const Joi = require('joi');

const schema = Joi.object().keys({
    salutation: Joi.string().required(),
    first_name: Joi.string().required(),
    middle_name: Joi.string().required(),
    last_name: Joi.string().required(),
    email: Joi.string().required(),
    password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
    is_active: Joi.string().required(),
    created_by: Joi.number()
})
module.exports = schema;