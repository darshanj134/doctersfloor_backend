'use strict';
module.exports = healthCheck;
function healthCheck(req, res,jwt,config) {
    console.info('Requesting api healthCheck!');
    var token = jwt.sign({ id: "1" }, config.secret_key, {
        expiresIn: 5400 // expires in 1.5 hours
    });
    let req_token   =   req.headers['x-access-token'];
    res.status(200).send({
        auth: true,
        token: token,
        status: 'OK',
        body: req.body,
        query: req.query,
        headers: req.headers.ab_authorization,
        'x-access-token':req_token,
        params: req.params,
        method: req.method
    });
}